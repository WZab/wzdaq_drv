# Version hls_dma

This version is prepared for working with the HLS-designed
DMA engine: https://gitlab.com/WZabISE/hls_dma

# Communication with application

The application must:

- first set the size of the hugapages DAQ1_IOC_SET_HPSHIFT
- set the number of hugepages that will be used for buffer DAQ1_IOC_SET_NOF_HP
  (should allocate one more for event descriptors)
- setup the buffers (DAQ1_IOC_SET_DMABUF)
- start the acquisition (DAQ1_IOC_START)

Now the application may start waiting for data
The application may voluntarily sleep, waiting for at least one event
to be ready (DAQ1_IOC_WAIT)


