import os
import mmap
import struct
import logging as log

class bus(object):
  def __init__(self, devfile="/dev/my_ctrl0"):
    # when using wojtek's driver "/dev/ax1ctl0"

    # O_SYNC disables output buffering
    # we don't need flushes
    self.f=os.open(devfile, os.O_RDWR | os.O_SYNC)

    # The ctrl file may be open only once. Therefore we can't use
    # os.path.size to find the size of the area. We have to use lseek.
    fsize = os.lseek(self.f,0,os.SEEK_END)
    # File position is not used, but maybe it will change?
    # Let's set it to the beginnning...
    os.lseek(self.f,0,os.SEEK_SET)
    self.m=mmap.mmap(self.f, fsize, flags=mmap.MAP_SHARED)
    
    # this is required in order to use 32 bit writes
    # more over reads and writes are generated once
    # (when using a simple write we get
    # 2 identical operations with short interval)
    self.mv=memoryview(self.m).cast('I')
    
  def read(self, addr):
    log.debug(f"Reading register 0x{'{:08x}'.format(addr)}")
    v = self.mv[addr]
    log.debug(f"Read value: {v} (0x{'{:08x}'.format(v)}) (0b{'{:032b}'.format(v)})")
    return v

  def readb(self, addr):
    v = self.read(addr)
    return lambda : v

  def write(self, addr, val):
    log.debug(f"Writing data 0x{'{:08x}'.format(val)} to address: 0x{'{:08x}'.format(addr)}")
    self.mv[addr]=val

  def writeb(self, addr, val):
    self.write(addr, val)

  def write_masked(self,addr,mask,val):
    log.debug(f"Writing data 0x{val:08x} to bits: 0x{mask:08x} address: 0x{addr:08x}")
    prev = self.read(addr)
    new = (prev & (0xffff_ffff-mask))|(val & mask)
    log.debug(f"Prev: 0x{prev:08x}, new: 0x{new:08x}")
    self.write(addr, new )

  def dispatch(self):
    pass
