/*
  This is a very simple program showing functionality of the WZDAQ1 device model
  
  Copyright (C) 2021 by Wojciech M. Zabolotny  wzab<at>ise.pw.edu.pl
  Licensed under GPL v2
    
  Development of the driver and application is partially supported by the
  European Union’s Horizon 2020 research and innovation programme
  under grant agreement No 871072.

*/
#include <stdio.h>
#include <sys/types.h>
#include <stdint.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <poll.h>
#include <time.h>
#include <sys/ioctl.h>
#include "wzab_daq1.h"
#include "wzdaq_drv.h"
#include "wzdaq_app.h"


//volatile WzDaq1Regs * regs = NULL;
int fdbuf = -1;
int fdev = -1;
uint32_t * mregs = NULL;
//volatile WzDaq1Regs * regs = NULL;

void get_and_process_data(int fbuf, char * hbuf)
{
  uint32_t exp_event = 0;
  const volatile uint64_t * dbuf = (const volatile uint64_t *) hbuf;
  const volatile uint64_t * events = (const volatile uint64_t *) &hbuf[NOF_HP * HP_SIZE];
  struct pollfd pfds[2];
  //Now we wait either for pressing a key or for new data
  pfds[0].fd = 0;  //stdin
  pfds[1].fd = fbuf;
  pfds[0].events = POLLIN;
  pfds[1].events = POLLIN | POLLERR;
  while(1) {
    int ready;
    ready = poll(pfds,2,-1);
    if(ready == -1) {
      perror("Error in waiting for data");
      return;
    } else if(ready == 0) {
      printf("That should not happen - poll returned 0\n");
      return;
    } else if(ready > 0) {
      if(pfds[0].revents & POLLIN) {       
        printf("A key pressed - leaving\n");
        return;
      }
      if(pfds[1].revents & POLLERR) {
	//Error (overrun?)
	printf("Error detected");
	return;
      }
      // Here we have data to be serviced
      while(1) {
	int evnt = ioctl(fbuf,DAQ1_IOC_GET_READY_DESC,0);
	if(evnt == -1) {
	  //Enable interrupts
	  ioctl(fbuf,DAQ1_IOC_CTRL,DAQ1_CMD_ENA_IRQ);
	  //leave the inner loop
	  break;
	} else {
	if(evnt != exp_event) {
	   printf("Expected event %d, received evend %d\n", exp_event, evnt);
	   return;
	}
	exp_event = ( exp_event + 1 ) % DAQ1_NUM_EVT_DESCS;
	  // We have an event to be serviced
	  // Synchronize the buffer
	  ioctl(fbuf,DAQ1_IOC_SYNC,evnt);
	  //Get access to the descriptor
	  const volatile uint64_t * desc = &events[evnt*4];
	  //DMA knows nothing about the computer's endianness - we have to use le64toh.
	  int afirst= le64toh(desc[0]);
	  int alast = le64toh(desc[1]);
	  int w2 = le64toh(desc[2]);
	  int w3 = le64toh(desc[3]);
	  if ((evnt % 0x100) == 0) printf("Evt:%d, first:%x, last:%x, w2=%x, w3=%x\n", evnt, afirst, alast, w2, w3);
	  //Confirm the event (It would be good to verify, that the value is correct
	  //But who can do that? App has no access to registers, driver has no access
	  //To the data buffer...
	  ioctl(fbuf,DAQ1_IOC_CONFIRM,alast);
	}
      }
    }
  }
}

int main(int argc, char * argv[])
{
  unsigned long val;
  int i;
  int res;
  char * hbuf;
  int fbuf = -1;
  int fdev = -1;
  int nrdev;
  char hpname[100];
  char devname[100];
  long int bufsize = (NOF_HP + 1) * HP_SIZE;
  snprintf(hpname,100,"/tmp/hugepages/tst%s",argv[1]);
  snprintf(devname,100,"/dev/my_daq%s",argv[1]);
  // Allocate the hugepages-backed buffer
  fbuf=open(hpname, O_CREAT | O_RDWR ,0600);
  if(fbuf == -1)
    {
      perror("I can't open HP file!\n");
      fflush(stdout);
      exit(1);
    }
  //Now set its size to 256 HPs
  res = ftruncate(fbuf,bufsize);
  if(res==-1)
    {
      perror("I can't resize HP file!\n");
      fflush(stdout);
      exit(1);
    }
  //Now mmap this file
  hbuf = (char *) mmap(NULL,bufsize,PROT_READ , MAP_PRIVATE | MAP_ANONYMOUS | MAP_HUGETLB, fbuf, 0);
  if (hbuf == MAP_FAILED)
    {
      perror("I can't mmap HP file!\n");
      fflush(stdout);
      exit(1);
    }
  //Now send the buffer to our device
  printf("I'm trying to open our device!\n");
  fflush(stdout);
  /* There are problems with accessing the registers from userspace.
   * Accessing them via devmem2 required booting the kernel with "ioremap=relaxed" option.
   */
  fdev=open(devname, O_RDWR | O_DSYNC | O_SYNC);
  if(fdev==-1)
    {
      perror("I can't open device!\n");
      fflush(stdout);
      exit(1);
    }
  //Reset the device to clear overruns
  res = ioctl(fdev,DAQ1_IOC_CTRL,DAQ1_CMD_DO_RESET);
  //MMap registers (for debugging!)
  mregs = (uint32_t *) mmap(NULL,AXI_MMAP_LEN,PROT_READ | PROT_WRITE, MAP_SHARED  , fdev, 0);
  if (mregs == MAP_FAILED)
    {
      perror("I can't mmap REGS!\n");
      fflush(stdout);
      exit(1);
    }
  /*
  printf("I'm trying to initialize the DMA core!\n");
  res = ioctl(fdev,DAQ1_IOC_CTRL,DAQ1_CMD_INIT);
  if(res==-1)
    {
      perror("I can't initialize the core!\n");
      fflush(stdout);
      exit(1);
    }

  //regs = (volatile WzDaq1Regs *) mregs;
  //Perform the DMA mapping of the allocated buffer
  //[!!! In that form we don't know how to pass the length of the buffer!]
  //Maybe we should put the length into the register?
  printf("I'm trying to set HP_SHIFT!\n");
  res = ioctl(fdev,DAQ1_IOC_SET_HPSHIFT,HP_SHIFT);
  if(res==-1)
    {
      perror("I can't set HPSHIFT!\n");
      fflush(stdout);
      exit(1);
    }
  */
  //printf("hp_shift set to %d\n",regs->hpshft);
  // Viewing of mmapped registers in GDB doesn't work, as described in
  // https://stackoverflow.com/questions/3640095/gdb-cant-access-mmapd-kernel-allocated-memory
  // 
  printf("I'm trying to set number of HP!\n");
  res = ioctl(fdev,DAQ1_IOC_SET_NOF_HP,NOF_HP);
  if(res==-1)
    {
      perror("I can't set NOF_HP!\n");
      fflush(stdout);
      exit(1);
    }
  printf("I'm trying to set DMA buffer!\n");
  res = ioctl(fdev,DAQ1_IOC_SET_DMABUF,hbuf);
  if(res==-1)
    {
      perror("I can't DMA map the HP buffer!\n");
      fflush(stdout);
      exit(1);
    }
  /*
  // TBD! Complete configuration, start acquisition!
  printf("Doing reset before start\n");
  res = ioctl(fdev,DAQ1_IOC_CTRL,DAQ1_CMD_DO_RESET);    
  if(res==-1)
    {
      perror("I can't reset the engine!\n");
      fflush(stdout);
      exit(1);
    }
  */
  printf("I'm trying to start engine!\n");
  res = ioctl(fdev,DAQ1_IOC_CTRL,DAQ1_CMD_START);
  if(res==-1)
    {
      perror("I can't start engine!\n");
      fflush(stdout);
      exit(1);
    }
  // Switch on interrupts
  printf("I'm trying to switch on IRQ!\n");
  res = ioctl(fdev,DAQ1_IOC_CTRL,DAQ1_CMD_ENA_IRQ);
  if(res==-1)
    {
      perror("I can't enable interrupts!\n");
      fflush(stdout);
      exit(1);
    }
  //Call the data acquisition function
  get_and_process_data(fdev,hbuf);
  // Stop the data acquisition
  res = ioctl(fdev,DAQ1_IOC_CTRL,DAQ1_CMD_DIS_IRQ);
  if(res==-1)
    {
      perror("I can't disable IRQ!\n");
      fflush(stdout);
      exit(1);
    }
  res = ioctl(fdev,DAQ1_IOC_CTRL,DAQ1_CMD_STOP);
  if(res==-1)
    {
      perror("I can't stop engine!\n");
      fflush(stdout);
      exit(1);
    }
  res = ioctl(fdev,DAQ1_IOC_RM_DMABUF,hbuf);
  if(res==-1)
    {
      perror("I can't DMA unmap the HP buffer!\n");
      fflush(stdout);
      exit(1);
    }
  munmap(hbuf,bufsize);
  close(fbuf);
  munmap(mregs,AXI_MMAP_LEN);
  close(fdev);
}
