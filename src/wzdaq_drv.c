/* Experimental WZDAQ1 device driver
 *
 * Copyright (C) 2021 by Wojciech M. Zabolotny
 * wzab<at>ise.pw.edu.pl
 * Significantly based on multiple drivers included in
 * sources of Linux
 * Therefore this source is licensed under GPL v2
 *
 * Many new solutions are taken from uio.c code.
 *
 * Development of this driver is partially supported by the
 * European Union’s Horizon 2020 research and innovation programme
 * under grant agreement No 871072.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/uaccess.h>
#include <linux/version.h>
MODULE_LICENSE("GPL v2");
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/sched.h>
#include <linux/mm.h>
#include <linux/vmalloc.h>
#include <linux/idr.h>
#include <linux/pci.h>
#include <linux/poll.h>
#include <linux/delay.h>
#include <linux/printk.h>

#include <asm/io.h>
#include <linux/interrupt.h>
#include <asm/uaccess.h>
#include "wzdaq_drv.h"
#include "wzab_daq1.h"


//It seems that accessing the 64-bit values via PCIe does not work reliably in AARCH64 architecture
static inline void wz_writeq(uint64_t val, uint64_t volatile __iomem * addr)
{
   void volatile __iomem * taddr = addr;
   writel(val & 0xffffffff, taddr);
   writel((val >> 32) & 0xffffffff, taddr+4);
}

static inline uint64_t wz_readl(uint64_t __iomem * addr)
{
   void __iomem * taddr = addr;
   return readl(taddr) | (((uint64_t) readl(taddr+4)) << 32);
}

//PCI IDs below are not registred! Use only for experiments!
#define PCI_VENDOR_ID_WZAB 0x32ab
#define PCI_DEVICE_ID_WZAB_WZDAQ1 0x8018
//Old IDs
//#define PCI_VENDOR_ID_WZAB 0xabba
//#define PCI_DEVICE_ID_WZAB_WZDAQ1 0x3342
#define DAQ1_MAX_DEVICES 16

//We need a driver that works with multiple boards
static int daq1_major = 0;
static int ctrl1_major = 0;
static struct cdev * daq1_cdev = NULL;
static struct cdev * ctrl1_cdev = NULL;
static DEFINE_IDR(daq1_idr);
static DEFINE_MUTEX(idr_lock);
static struct class daq1_class = {
            .name = "wz_hls_daq1_class",
    };
static bool daq1_class_registered = 0;
static struct class ctrl1_class = {
            .name = "wz_hls_ctrl1_class",
    };
static bool ctrl1_class_registered = 0;

static const struct file_operations daq1_fops;
static const struct file_operations ctrl1_fops;

struct daq1_device {
    int minor;
    struct mutex is_open;
    struct mutex ctrl_is_open;
    uint64_t phys_addr;
    uint64_t agwb_addr;
    uint64_t agwb_size;
    volatile void * fmem;
    uint64_t __user * descs;
    unsigned long * desc_to_confirm; //Bitmap of length DAQ1_NUM_EVT_DESCS
    struct mutex confirm_lock;
    spinlock_t gpio_lock;
    struct pci_dev * pdev;
    wait_queue_head_t wait;
    int irq;
    //DMA buffer related data
    struct sg_table sgt;
    struct page ** pages;
    struct scatterlist ** sgtrans;
    int npages;
};

static const struct pci_device_id daq1_pci_tbl[] = {
    {PCI_DEVICE(PCI_VENDOR_ID_WZAB, PCI_DEVICE_ID_WZAB_WZDAQ1)},
    {}
};
MODULE_DEVICE_TABLE(pci, daq1_pci_tbl);

#define SUCCESS 0
#define DEVICE_NAME "wzab_daq1"

void cleanup_daq1( void );
void cleanup_daq1( void );
int init_daq1( void );

void sgl_unmap(struct daq1_device * ddev);
static int daq1_get_minor(struct daq1_device *idev);

static int daq1_open(struct inode *inode, struct file *file);
static int daq1_release(struct inode *inode, struct file *file);

static long daq1_ioctl(struct file *filp, unsigned int cmd, unsigned long arg);

int daq1_mmap(struct file *filp, struct vm_area_struct *vma);

static int ctrl1_open(struct inode *inode, struct file *file);
static int ctrl1_release(struct inode *inode, struct file *file);
static int ctrl1_mmap(struct file *filp, struct vm_area_struct *vma);
static loff_t ctrl1_llseek(struct file *filp, loff_t offset, int whence);

/* Queue for reading process */

/* Interrupt service routine */
irqreturn_t daq1_irq(int irq, void * dev_id)
{
    uint32_t status, ctrl;
    struct daq1_device * ddev = dev_id;
#ifdef WZDAQ_DEBUG
    printk(KERN_DEBUG "irq returns\n");
#endif
    //Read the status from GPIO
    spin_lock(&ddev->gpio_lock);
    status = * (uint32_t *) (ddev->fmem + AXI_CTRL_IND);
    spin_unlock(&ddev->gpio_lock);
    if (status & (
                (1 << CTRL_IND_BIT_OVERRUN) |
                (1 << CTRL_IND_BIT_PKTAV)
            )) {
        //Yes, this is our device
        //Block interrupts, they will be turned on by the reading process
        spin_lock(&ddev->gpio_lock);
        ctrl = * (uint32_t *) (ddev->fmem + AXI_CTRL_OUTD);
        ctrl &= ~(1 << CTRL_OUTD_BIT_IRQ_ENA);
        * (uint32_t *) (ddev->fmem + AXI_CTRL_OUTD) = ctrl;
        spin_unlock(&ddev->gpio_lock);
        //Wake up the reading process
        wake_up_interruptible(&ddev->wait);
        return IRQ_HANDLED;
    }
    return IRQ_NONE; //Our device does not request interrupt
};


__poll_t daq1_poll(struct file *filp,poll_table *wait)
{
    uint32_t status;
    unsigned long flags;
    __poll_t mask = 0;
    struct daq1_device * ddev;
    int res;
    ddev = filp->private_data;
    if (!ddev) {
        res = -ENODEV;
        return res;
    }
    poll_wait(filp,&ddev->wait,wait);
    spin_lock_irqsave(&ddev->gpio_lock, flags);
    status = * (uint32_t *) (ddev->fmem + AXI_CTRL_IND);
    spin_unlock_irqrestore(&ddev->gpio_lock, flags);
    // Here we check if the new data arrived
    if(status & (1 << CTRL_IND_BIT_PKTAV))
        mask |= POLLIN |POLLRDNORM;
    // Here I should check for the overrun (TBM!)
    if(status & (1 << CTRL_IND_BIT_OVERRUN))
        mask |= POLLERR;
#ifdef WZDAQ_DEBUG
    printk(KERN_DEBUG "poll returns %d\n",mask);
#endif
    return mask;
}


static const struct file_operations daq1_fops = {
    .owner = THIS_MODULE,
    //.read=daq1_read, /* read */
    //.write=daq1_write, /* write */
    .unlocked_ioctl=daq1_ioctl,
    .open=daq1_open,
    .release=daq1_release,  /* a.k.a. close */
    .llseek=no_llseek,
    .poll=daq1_poll,
    .mmap=daq1_mmap
};

static const struct file_operations ctrl1_fops = {
    .owner = THIS_MODULE,
    //.read=ctrl1_read, /* read */
    //.write=ctrl1_write, /* write */
    //.unlocked_ioctl=ctrl1_ioctl,
    .open=ctrl1_open,
    .release=ctrl1_release,  /* a.k.a. close */
    .llseek=ctrl1_llseek,
    //.poll=ctrl1_poll,
    .mmap=ctrl1_mmap
};

static loff_t ctrl1_llseek(struct file *filp, loff_t offset, int whence)
{
    struct daq1_device * daq1_dev;
    daq1_dev = filp->private_data;
    if (!daq1_dev) {
        return -ENODEV;
    }
    return fixed_size_llseek(filp, offset, whence, daq1_dev->agwb_size);
}

static int ctrl1_open(struct inode *inode,
                      struct file *file)
{
    int res=0;
    struct daq1_device * daq1_dev;
    //nonseekable_open(inode, file);
    mutex_lock(&idr_lock);
    daq1_dev = idr_find(&daq1_idr, iminor(inode));
    mutex_unlock(&idr_lock);
    if (!daq1_dev) {
        res = -ENODEV;
        goto out;
    }
    if (!mutex_trylock(&daq1_dev->ctrl_is_open))
        return -EBUSY;
    file->private_data = daq1_dev;
    return SUCCESS;
out:
    return res;
}

static int ctrl1_release(struct inode *inode,
                         struct file *file)
{
    struct daq1_device * daq1_dev;
    int res;
#ifdef WZDAQ_DEBUG
    printk(KERN_DEBUG "I'm in release...");
#endif
    mutex_lock(&idr_lock);
    daq1_dev = idr_find(&daq1_idr, iminor(inode));
    mutex_unlock(&idr_lock);
    if (!daq1_dev) {
        res = -ENODEV;
        goto out;
    }
#ifdef WZDAQ_DEBUG
    printk (KERN_DEBUG "device_release(%p,%p)\n", inode, file);
#endif
    //Disable IRQ
    mutex_unlock(&daq1_dev->ctrl_is_open);
    return SUCCESS;
out:
    return res;
}

static struct vm_operations_struct ctrl1_vm_ops = {
    //Taken from uio.c from kernel 5.12
    // Without that, I couldn't access the registers from gdb
    // However, anyway the access from gdb gives wrong results!
    // Read returns 0, Write does nothing?
#ifdef CONFIG_HAVE_IOREMAP_PROT
    .access = generic_access_phys,
#endif
};

//Please note, that's used for registers that are mapped using normal pages
int ctrl1_mmap(struct file *filp,
               struct vm_area_struct *vma)
{
    struct daq1_device * daq1_dev;
    unsigned long off;
    //int res;
    daq1_dev = filp->private_data;
    off = vma->vm_pgoff << PAGE_SHIFT;
    if(off==0) {
        //Mapping of registers
        unsigned long physical = daq1_dev->agwb_addr;
        unsigned long vsize = vma->vm_end - vma->vm_start;
        unsigned long psize = daq1_dev->agwb_size; //AGWB-managed area
        if(vsize>psize)
            return -EINVAL;
        vma->vm_page_prot=pgprot_noncached(vma->vm_page_prot);
        vma->vm_ops = &ctrl1_vm_ops;
#ifdef WZDAQ_DEBUG
        printk(KERN_DEBUG "Mapping regs virt=%lx phys=%lx\n",vma->vm_start, physical);
#endif
        return io_remap_pfn_range(vma,vma->vm_start, physical >> PAGE_SHIFT, vsize, vma->vm_page_prot);
    } else {
        return -EINVAL;
    }
}

/* Cleanup resources */
void daq1_remove(struct pci_dev *pdev )
{
    struct daq1_device * daq1_dev;

    daq1_dev = pci_get_drvdata(pdev);
    if(! daq1_dev) return;
    pci_set_drvdata(pdev,NULL);
    device_destroy(&daq1_class,MKDEV(daq1_major,daq1_dev->minor));
    device_destroy(&ctrl1_class,MKDEV(ctrl1_major,daq1_dev->minor));
    if(daq1_dev->fmem) iounmap(daq1_dev->fmem);
    pci_release_regions(pdev);
    pci_disable_device(pdev);
    kfree(daq1_dev);
}

static int daq1_open(struct inode *inode,
                     struct file *file)
{
    int res=0;
    struct daq1_device * daq1_dev;
    nonseekable_open(inode, file);
    mutex_lock(&idr_lock);
    daq1_dev = idr_find(&daq1_idr, iminor(inode));
    mutex_unlock(&idr_lock);
    if (!daq1_dev) {
        res = -ENODEV;
        goto out;
    }
    if (!mutex_trylock(&daq1_dev->is_open))
        return -EBUSY;
    file->private_data = daq1_dev;
    res=request_irq(daq1_dev->irq,daq1_irq,IRQF_SHARED,DEVICE_NAME,daq1_dev); //Should be changed for multiple WZENC1s
    if(res) {
        printk (KERN_ALERT "wzab_daq1: I can't connect irq %i error: %d\n", daq1_dev->irq,res);
        daq1_dev->irq = -1;
    }
    return SUCCESS;
out:
    return res;
}

static int daq1_release(struct inode *inode,
                        struct file *file)
{
    struct daq1_device * daq1_dev;
    int res;
    uint32_t ctrl;
    unsigned long flags;
#ifdef WZDAQ_DEBUG
    printk(KERN_DEBUG "I'm in release...");
#endif
    mutex_lock(&idr_lock);
    daq1_dev = idr_find(&daq1_idr, iminor(inode));
    mutex_unlock(&idr_lock);
    if (!daq1_dev) {
        res = -ENODEV;
        goto out;
    }
#ifdef WZDAQ_DEBUG
    printk (KERN_DEBUG "device_release(%p,%p)\n", inode, file);
#endif
    //Disable IRQ
    spin_lock_irqsave(&daq1_dev->gpio_lock, flags);
    ctrl = * (uint32_t *) (daq1_dev->fmem + AXI_CTRL_OUTD);
    ctrl &= ~(1 << CTRL_OUTD_BIT_IRQ_ENA);
    ctrl &= ~(1 << CTRL_OUTD_BIT_AP_START);
    ctrl &= ~(1 << CTRL_OUTD_BIT_SRC_START);
    * (uint32_t *) (daq1_dev->fmem + AXI_CTRL_OUTD) = ctrl;
    spin_unlock_irqrestore(&daq1_dev->gpio_lock, flags);
    if(daq1_dev->irq>=0) free_irq(daq1_dev->irq,daq1_dev); //Free interrupt
    //If the memory was mapped, unmap it!
    //Of course we should disable the device first!
    sgl_unmap(daq1_dev);
    mutex_unlock(&daq1_dev->is_open);
    return SUCCESS;
out:
    return res;
}

static struct vm_operations_struct daq1_vm_ops = {
    //Taken from uio.c from kernel 5.12
    // Without that, I couldn't access the registers from gdb
    // However, anyway the access from gdb gives wrong results!
    // Read returns 0, Write does nothing?
#ifdef CONFIG_HAVE_IOREMAP_PROT
    .access = generic_access_phys,
#endif
};

//Please note, that's used for registers that are mapped using normal pages
int daq1_mmap(struct file *filp,
              struct vm_area_struct *vma)
{
    struct daq1_device * daq1_dev;
    unsigned long off;
    //int res;
    daq1_dev = filp->private_data;
    off = vma->vm_pgoff << PAGE_SHIFT;
    if(off==0) {
        //Mapping of registers
        unsigned long physical = daq1_dev->phys_addr;
        unsigned long vsize = vma->vm_end - vma->vm_start;
        unsigned long psize = AXI_MMAP_LEN; //Registers and addresses of hugepages
        if(vsize>psize)
            return -EINVAL;
        vma->vm_page_prot=pgprot_noncached(vma->vm_page_prot);
        vma->vm_ops = &daq1_vm_ops;
#ifdef WZDAQ_DEBUG
        printk(KERN_DEBUG "Mapping regs virt=%lx phys=%lx\n",vma->vm_start, physical);
#endif
        return io_remap_pfn_range(vma,vma->vm_start, physical >> PAGE_SHIFT, vsize, vma->vm_page_prot);
    } else {
        return -EINVAL;
    }
}

int sgl_map(struct daq1_device * ddev, const char __user *buf, size_t count)
{
    int res = 0;
    void * resptr __attribute__((unused)) = NULL; //For functions returning pointers (may be not used in certain kernels)
    int n_pages = 0;
    struct page ** pages = NULL;
    const unsigned long offset = ((unsigned long)buf) & (PAGE_SIZE-1);
    //Calculate number of pages
    n_pages = (offset + count + PAGE_SIZE - 1) >> PAGE_SHIFT;
#ifdef WZDAQ_DEBUG
    printk(KERN_DEBUG "n_pages: %d",n_pages);
#endif
    //Allocate the table for pages
    pages = vzalloc(sizeof(* pages) * n_pages);
#ifdef WZDAQ_DEBUG
    printk(KERN_DEBUG "pages: %p",pages);
#endif
    if(pages == NULL) {
        res = -ENOMEM;
        goto sglm_err1;
    }
    //Now pin the pages
    res = get_user_pages_fast(((unsigned long)buf & PAGE_MASK), n_pages, 0, pages);
#ifdef WZDAQ_DEBUG
    printk(KERN_DEBUG "gupf: %d",res);
#endif
    if(res < n_pages) {
        int i;
        for(i=0; i<res; i++)
            put_page(pages[i]);
        res = -ENOMEM;
        goto sglm_err0;
    }
    //Now create the sg-list - the function depends on the kernel version!
    //The strict number of the version is not set correctly yet
#if LINUX_VERSION_CODE < KERNEL_VERSION(5,10,0)
    res = __sg_alloc_table_from_pages(&ddev->sgt, pages, n_pages, offset, count, HP_SIZE, GFP_KERNEL);
#elif LINUX_VERSION_CODE < KERNEL_VERSION(5,15,0)
    resptr = __sg_alloc_table_from_pages(&ddev->sgt, pages, n_pages, offset, count, HP_SIZE, NULL, 0, GFP_KERNEL);
    res = PTR_ERR_OR_ZERO(resptr);
#else
    //The new kernel
    res = sg_alloc_table_from_pages_segment(&ddev->sgt, pages, n_pages, offset, count, HP_SIZE, GFP_KERNEL);
#endif
#ifdef WZDAQ_DEBUG
    printk(KERN_DEBUG "satf: %d",res);
#endif
    if(res < 0)
        goto sglm_err2;
    ddev->pages = pages;
    ddev->npages = n_pages;
    return res;
sglm_err2:
    //Here we jump if we know that the pages are pinned
    {
        int i;
        for(i=0; i<n_pages; i++)
            put_page(pages[i]);
    }
sglm_err1:
    sg_free_table(&ddev->sgt);
    memset(&ddev->sgt,0,sizeof(ddev->sgt));
sglm_err0:
    if(pages) vfree(pages);
    ddev->pages = NULL;
    ddev->npages = 0;
    return res;
}

void sgl_unmap(struct daq1_device * ddev)
{
    int i;
    //Free the sg list
    if(ddev->sgt.sgl) {
        sg_free_table(&ddev->sgt);
        memset(&ddev->sgt,0,sizeof(ddev->sgt));
    }
    if(ddev->pages) {
        //Unpin pages
        for(i=0; i < ddev->npages; i++) {
            set_page_dirty(ddev->pages[i]);
            put_page(ddev->pages[i]);
        }
        vfree(ddev->pages);
        ddev->pages = NULL;
    }
    //Clear SG addresses table
    if(ddev->sgtrans) {
        vfree(ddev->sgtrans);
        ddev->sgtrans = NULL;
    }
    //Clear the descs address
    ddev->descs = NULL;
    if(ddev->desc_to_confirm) {
        bitmap_free(ddev->desc_to_confirm);
        ddev->desc_to_confirm = NULL;
    }
}

static inline int daq_ready(struct daq1_device * ddev)
{
    unsigned long flags;
    uint32_t status;
    spin_lock_irqsave(&ddev->gpio_lock, flags);
    status = * (uint32_t *) (ddev->fmem + AXI_CTRL_IND);
    spin_unlock_irqrestore(&ddev->gpio_lock, flags);
    // Here we check if the new data arrived
    if(status & (1 << CTRL_IND_BIT_PKTAV))
        return 1;
    // Here I should check for the overrun (TBM!)
    if(status & (1 << CTRL_IND_BIT_OVERRUN))
        return 2;
    return 0;
}

static int wz_enable_irq(struct daq1_device *ddev)
{
    unsigned long irqstate;
    uint32_t ctrl;
    spin_lock_irqsave(&ddev->gpio_lock,irqstate);
    ctrl = * (uint32_t *) (ddev->fmem + AXI_CTRL_OUTD);
    ctrl |= (1 << CTRL_OUTD_BIT_IRQ_ENA);
    * (uint32_t *) (ddev->fmem + AXI_CTRL_OUTD) = ctrl;
    spin_unlock_irqrestore(&ddev->gpio_lock,irqstate);
    return 0;
}

static int wz_disable_irq(struct daq1_device *ddev)
{
    unsigned long irqstate;
    uint32_t ctrl;
    spin_lock_irqsave(&ddev->gpio_lock,irqstate);
    ctrl = * (uint32_t *) (ddev->fmem + AXI_CTRL_OUTD);
    ctrl &= ~(1 << CTRL_OUTD_BIT_IRQ_ENA);
    * (uint32_t *) (ddev->fmem + AXI_CTRL_OUTD) = ctrl;
    spin_unlock_irqrestore(&ddev->gpio_lock,irqstate);
    return 0;
}


static long daq1_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
    struct daq1_device * ddev;
    int res,i, nof_hp;
    ddev = filp->private_data;
    switch(cmd)
    {
    case DAQ1_IOC_SET_DMABUF:
        //If the buffer was already mapped (it may happen in case of restart),
        //unmap the buffer first. We do not reuse the old buffer (e.g., the
        //length of the buffer may be changed?)
        if(ddev->descs) {
            sgl_unmap(ddev);
        }
        //Create the SG table
        //Get the number of huge pages from the register
        nof_hp = * (uint32_t *)  (ddev->fmem + DAQ1_NOF_BUFS);
        res = sgl_map(ddev,(const char *)arg,(nof_hp+1)*HP_SIZE);
        if(res) {
            sgl_unmap(ddev);
            return res;
        }
        //Create the SG address translation table (needed for buffer synchronization)
        //as the scatterlist table can't be indexed
        {
            struct scatterlist * *s = (struct scatterlist * *) vzalloc(sizeof(struct scatterlist *) * nof_hp+1);
            if(s == NULL) {
                sgl_unmap(ddev);
                return -ENOMEM;
            }
            ddev->sgtrans = s; //The table will be filled later
        }
#ifdef WZDAQ_DEBUG
        {
            struct scatterlist * s = ddev->sgt.sgl;
            printk(KERN_DEBUG "Early report sgt: nents=%d, orig_nents=%d\n", ddev->sgt.nents, ddev->sgt.orig_nents);
            for(i = 0; i < ddev->sgt.orig_nents ; i++) {
                printk(KERN_DEBUG "n: %d , pl=%lx length=%x\n",i,s->page_link,s->length);
                s = sg_next(s);
            }
        }
#endif
        //Create the mapping for the device
        res = dma_map_sg_attrs(&ddev->pdev->dev,ddev->sgt.sgl,ddev->sgt.nents,DMA_FROM_DEVICE,DMA_ATTR_NO_KERNEL_MAPPING);
        if(!res) {
            sgl_unmap(ddev);
            return res;
        }
        //Now print the number of nents and their size...
#ifdef WZDAQ_DEBUG
        {
            int i;
            struct scatterlist * s = ddev->sgt.sgl;
            printk(KERN_INFO "sgt: nents=%d, orig_nents=%d\n", ddev->sgt.nents, ddev->sgt.orig_nents);
            for(i = 0; i < ddev->sgt.orig_nents ; i++) {
                printk(KERN_INFO "n: %d , pl=%lx length=%x\n",i,s->page_link,s->length);
                printk(KERN_INFO "n: %d , dma=%llx length=%x\n",i,s->dma_address,s->dma_length);
                s = sg_next(s);
            }
        }
#endif
        //Make sure that the number of hugepages is correct
        if(ddev->sgt.nents != (nof_hp + 1)) {
            printk(KERN_ERR "The original number of scattterlist entries: %d has changed to %d\n",
                   nof_hp + 1, ddev->sgt.nents);
            sgl_unmap(ddev);
            return -EINVAL;
        }
        //Now write the physical extent locations to the registers
        {
            struct scatterlist * s = ddev->sgt.sgl;
            for(i = 0 ; i < nof_hp ; i++) {
                volatile uint64_t * hps = (volatile uint64_t *) ((ddev->fmem) + DAQ1_BUFS);
                wz_writeq(s->dma_address, &hps[i]);
                ddev->sgtrans[i] = s;
                s = sg_next(s);
            }
            //Now assign the last huge page to the packet decriptors
            ddev->sgtrans[nof_hp] = s;
            wz_writeq(s->dma_address, (uint64_t __iomem *) (ddev->fmem + DAQ1_DESCS));
            //And store its virtual address in the device data
            ddev->descs = (uint64_t __user *) ((unsigned char *) arg + nof_hp * HP_SIZE);
        }
        ddev->desc_to_confirm = bitmap_zalloc(DAQ1_NUM_EVT_DESCS,GFP_KERNEL);
        if(!ddev->desc_to_confirm) {
            printk(KERN_ERR "I can't allocate the descriptor confirmation bitmap\n");
            sgl_unmap(ddev);
            return -ENOMEM;
        }
        //The buffers are set up.
        return 0;
    case DAQ1_IOC_SYNC:
        //This ioctl synchronizes the part of DMA buf associated with the particular packet
        //for access by CPU.
        //The argument should be set to the number of the packet
    {
        struct scatterlist * * s = NULL;
        uint64_t * descp;
        uint64_t first;
        uint64_t after;
        int firstbuf;
        int lastbuf;
        uint32_t nof_hp;
        uint32_t readp = arg;
        if(readp >= DAQ1_NUM_EVT_DESCS) {
            return -EINVAL;
        }
        s = ddev->sgtrans;
        nof_hp = * (uint32_t *)  (ddev->fmem + DAQ1_NOF_BUFS);
        //Synchronize the packet descriptors
        dma_sync_sg_for_cpu(&ddev->pdev->dev, s[nof_hp], 1, DMA_FROM_DEVICE);
        //Now we can read the numbers of involved buffers
        descp = &ddev->descs[readp*4];
        get_user(first,descp);
        descp += 1;
        get_user(after,descp);
        first = le64_to_cpu(first);
        after = le64_to_cpu(after);
#ifdef WZDAQ_DEBUG
        printk(KERN_DEBUG "Synchronizing: Evt:%d, first:%llx after:%llx\n",readp,first,after);
#endif
        //Synchronize the appropriate parts of the sgtable
        firstbuf = first / HP_SIZE_IN_WORDS;
        lastbuf = after / HP_SIZE_IN_WORDS;
        if(after > first) {
            //We need to synchronize a single range of huge pages
            dma_sync_sg_for_cpu(&ddev->pdev->dev, s[firstbuf], lastbuf-firstbuf+1, DMA_FROM_DEVICE);
        } else {
            //We need to synchronize two ranges
            dma_sync_sg_for_cpu(&ddev->pdev->dev, s[firstbuf], nof_hp - firstbuf, DMA_FROM_DEVICE);
            //The second range only if there is at least one word written
            if(after) {
                dma_sync_sg_for_cpu(&ddev->pdev->dev, s[0], lastbuf+1, DMA_FROM_DEVICE);
            }
        }
        return 0;
    }
    case DAQ1_IOC_WAIT:
    {
        int res = daq_ready(ddev);
        if(res==1) return 0;
        if(res==2) return -2;
        //No event, so enable interrupts and wait
        wz_enable_irq(ddev);
        res=wait_event_interruptible(ddev->wait,daq_ready(ddev));
        return res;
    }
    case DAQ1_IOC_RM_DMABUF:
    {
        // Currently the argument (the address of the buffer) is not used
        int res = 0;
        dma_unmap_sg_attrs(&ddev->pdev->dev,ddev->sgt.sgl,ddev->sgt.nents,DMA_FROM_DEVICE,DMA_ATTR_NO_KERNEL_MAPPING);
        sgl_unmap(ddev);
        ddev->descs = NULL;
        return res;
    }
    case DAQ1_IOC_SET_NOF_HP:
    {
        unsigned long irqstate;
        //Check that the number of hugepages is correct and modify it if not
        int nofhp = arg;
        if(nofhp > DAQ1_MAX_NOF_BUFS)
            nofhp = DAQ1_MAX_NOF_BUFS;
        spin_lock_irqsave(&ddev->gpio_lock,irqstate);
        * (uint32_t *)  (ddev->fmem + DAQ1_NOF_BUFS) = nofhp;
        spin_unlock_irqrestore(&ddev->gpio_lock,irqstate);
        //Return the accepted number of hugepages
        return nofhp;
    }
    case DAQ1_IOC_GET_READY_DESC:
    {
        unsigned long irqstate;
        uint32_t readp, writep;
        spin_lock_irqsave(&ddev->gpio_lock,irqstate);
        readp = * (uint32_t *)  (ddev->fmem + DAQ1_SRV_PKT);
        writep = * (uint32_t *)  (ddev->fmem + DAQ1_NR_PKT);
        spin_unlock_irqrestore(&ddev->gpio_lock,irqstate);
        if(writep == readp) {
            //Next event not ready
            return -1;
        } else {
            return readp;
        }
    }
    case DAQ1_IOC_GET_WRITTEN_DESC:
    {
        //This function returns the number of currently written packets.
        //It may be used for parallel processing of packets to check if
        //there is a new packet that should be scheduled for handling.
        unsigned long irqstate;
        uint32_t writep;
        spin_lock_irqsave(&ddev->gpio_lock,irqstate);
        writep = * (uint32_t *)  (ddev->fmem + DAQ1_NR_PKT);
        spin_unlock_irqrestore(&ddev->gpio_lock,irqstate);
        return writep;
    }
    case DAQ1_IOC_CONFIRM_SRV:
    {
        //This function confirms that the packet pointed by srv_pakt
        //has been scheduled for handling (it will be used in parallel
        //processing of packets)
        uint32_t srvp;
        unsigned long irqstate;
        spin_lock_irqsave(&ddev->gpio_lock,irqstate);
        srvp = * (uint32_t *)  (ddev->fmem + DAQ1_SRV_PKT);
        srvp = (srvp + 1) % DAQ1_NUM_EVT_DESCS;
        * (uint32_t *)  (ddev->fmem + DAQ1_SRV_PKT) = srvp;
        spin_unlock_irqrestore(&ddev->gpio_lock,irqstate);
        return 0;
    }
    case DAQ1_IOC_CONFIRM:
        // This function confirms completed processing of the packet
        // currently described by the descritor pointed by readp.
        // This function is useful only for sequential handling of packets.
    {
        unsigned long irqstate;
        uint32_t readp, nr_buf,nr_pkt;
        uint64_t nxt_word;
        uint64_t __user * descp;
        spin_lock_irqsave(&ddev->gpio_lock,irqstate);
        readp = * (uint32_t *)  (ddev->fmem + DAQ1_CUR_PKT);
        nr_buf = * (uint32_t *)  (ddev->fmem + DAQ1_NR_BUF);
        nr_pkt = * (uint32_t *)  (ddev->fmem + DAQ1_NR_PKT);
        //Remember that descs is the userspace pointer!
        //Therefore we need to access it via get_user.
        descp = &(ddev->descs[4*readp+1]);
        get_user(nxt_word,descp);
        //Find the number of the first unconfirmed buffer - divide the address
        //of the first uncorfirmed word by the numbers of words in the buffer.
        nxt_word /= DAQ1_BUFLEN_IN_WORDS;
        * (uint32_t *) (ddev->fmem + DAQ1_CUR_BUF) = nxt_word;
        //Increase the number of the packet and write it to both
        //registers - CUR_PKT and SRV_PKT
        readp = (readp + 1) % DAQ1_NUM_EVT_DESCS;
        * (uint32_t *)  (ddev->fmem + DAQ1_CUR_PKT) = readp;
        //In single packet processing cur_pkt and srv_pkt should
        //be equal all the time.
        * (uint32_t *)  (ddev->fmem + DAQ1_SRV_PKT) = readp;
        spin_unlock_irqrestore(&ddev->gpio_lock,irqstate);
#ifdef WZDAQ_DEBUG
        printk( KERN_DEBUG "Confirm: new_pkt=%x nr_pkt=%x new_buf=%llx nr_buf=%x",readp,nr_pkt,nxt_word,nr_buf);
#endif
        return 0;
    }
    case DAQ1_IOC_CONFIRM_THAT:
        // This function confirms completed processing of the descriptor pointed by the
        // argument.
        // If this is the descriptor pointed by "readp" it frees that and following ones
        // which have been already confirmed.
    {
        unsigned long irqstate;
        uint32_t readp, writep, thatp, nextp;
        uint64_t nxt_word;
        uint64_t __user * descp;
        uint32_t distw, distt;
        thatp = (uint32_t) arg;
        mutex_lock(&ddev->confirm_lock);
        //Synchronize the packet descriptors
        {
            struct scatterlist * * s = ddev->sgtrans;
            nof_hp = * (uint32_t *)  (ddev->fmem + DAQ1_NOF_BUFS);
            dma_sync_sg_for_cpu(&ddev->pdev->dev, s[nof_hp], 1, DMA_FROM_DEVICE);
        }
        spin_lock_irqsave(&ddev->gpio_lock,irqstate);
        readp = * (uint32_t *)  (ddev->fmem + DAQ1_CUR_PKT);
        writep = * (uint32_t *)  (ddev->fmem + DAQ1_NR_PKT);
        spin_unlock_irqrestore(&ddev->gpio_lock,irqstate);
        //We check that the required descriptor is in range from readp to writep (except writep)?
        distw = ((uint32_t) writep - (uint32_t) readp) % DAQ1_NUM_EVT_DESCS;
        distt = ((uint32_t) thatp - (uint32_t) readp) % DAQ1_NUM_EVT_DESCS;
        if(distt > distw) {
            mutex_unlock(&ddev->confirm_lock);
            return -EINVAL;
        }
        //If current packet is not the one pointed by readp, flag it as ready to be confirmed and return.
        if(thatp != readp) {
            set_bit(thatp,ddev->desc_to_confirm);
            mutex_unlock(&ddev->confirm_lock);
            return 0;
        }
        //In this case readp is equal thatp, so we can free at least one buffer.
        //We find the last buffer that should be confirmed.
        while(thatp != writep) {
            nextp = (thatp + 1) % DAQ1_NUM_EVT_DESCS;
            if(!test_bit(nextp,ddev->desc_to_confirm)) break;
            clear_bit(nextp,ddev->desc_to_confirm);
            thatp = nextp;
        }
        //Now we can free all packets until the one pointed by thatp
        //However, what if the packet pointed by nextp is confirmed in the meantime?
        //Then it will be never fried!
        //So we must make sure that no packet could be confirmed when we
        //scan for the last to be freed.
        //That's why confirm_lock mutex is used.
        //Remember that descs is the userspace pointer!
        //Therefore we need to access it via get_user.
        descp = &(ddev->descs[4*thatp+1]);
        get_user(nxt_word,descp);
        //Find the number of the first unconfirmed buffer - divide the address
        //of the first uncorfirmed word by the numbers of words in the buffer.
        nxt_word /= DAQ1_BUFLEN_IN_WORDS;
        spin_lock_irqsave(&ddev->gpio_lock,irqstate);
        * (uint32_t *) (ddev->fmem + DAQ1_CUR_BUF) = nxt_word;
        //Increase the number of the packet
        * (uint32_t *)  (ddev->fmem + DAQ1_CUR_PKT) = nextp;
        spin_unlock_irqrestore(&ddev->gpio_lock,irqstate);
        mutex_unlock(&ddev->confirm_lock);
#ifdef WZDAQ_DEBUG
        printk( KERN_DEBUG "Confirm: new_pkt=%x nr_pkt=%x new_buf=%llx",readp,thatp,nxt_word);
#endif
        return 0;
    }
    case DAQ1_IOC_CTRL:
        // Here I should handle different commands
        switch(arg) {
        case DAQ1_CMD_INIT:
        {
            unsigned long irqstate;
            uint32_t ctrl;
            spin_lock_irqsave(&ddev->gpio_lock,irqstate);
            ctrl = * (uint32_t *) (ddev->fmem + AXI_CTRL_OUTD);
            ctrl |= (1 << CTRL_OUTD_BIT_AP_START);
            * (uint32_t *) (ddev->fmem + AXI_CTRL_OUTD) = ctrl;
            spin_unlock_irqrestore(&ddev->gpio_lock,irqstate);
            return 0;
        }
        case DAQ1_CMD_DEINIT:
        {
            unsigned long irqstate;
            uint32_t ctrl;
            spin_lock_irqsave(&ddev->gpio_lock,irqstate);
            ctrl = * (uint32_t *) (ddev->fmem + AXI_CTRL_OUTD);
            ctrl &= ~(1 << CTRL_OUTD_BIT_AP_START);
            * (uint32_t *) (ddev->fmem + AXI_CTRL_OUTD) = ctrl;
            spin_unlock_irqrestore(&ddev->gpio_lock,irqstate);
            return 0;
        }
        case DAQ1_CMD_ENA_IRQ:
            return wz_enable_irq(ddev);
        case DAQ1_CMD_SET_RESET:
        {
            unsigned long irqstate;
            uint32_t ctrl;
            spin_lock_irqsave(&ddev->gpio_lock,irqstate);
            ctrl = * (uint32_t *) (ddev->fmem + AXI_CTRL_OUTD);
            ctrl &= ~(1 << CTRL_OUTD_BIT_AP_nRST);
            * (uint32_t *) (ddev->fmem + AXI_CTRL_OUTD) = ctrl;
            spin_unlock_irqrestore(&ddev->gpio_lock,irqstate);
            return 0;
        }
        case DAQ1_CMD_CLR_RESET:
        {
            unsigned long irqstate;
            uint32_t ctrl;
            spin_lock_irqsave(&ddev->gpio_lock,irqstate);
            ctrl = * (uint32_t *) (ddev->fmem + AXI_CTRL_OUTD);
            ctrl |= (1 << CTRL_OUTD_BIT_AP_nRST);
            * (uint32_t *) (ddev->fmem + AXI_CTRL_OUTD) = ctrl;
            spin_unlock_irqrestore(&ddev->gpio_lock,irqstate);
            return 0;
        }
        case DAQ1_CMD_DO_RESET:
        {
            unsigned long irqstate;
            uint32_t ctrl;
            spin_lock_irqsave(&ddev->gpio_lock,irqstate);
            ctrl = 0; //* (uint32_t *) (ddev->fmem + AXI_GPIO_CTRL_OUTD);
            //ctrl &= ~(1 << GPIO_OUTD_BIT_AP_nRST);
            * (uint32_t *)  (ddev->fmem + DAQ1_CUR_PKT) = 0;
            * (uint32_t *)  (ddev->fmem + DAQ1_SRV_PKT) = 0;
            * (uint32_t *)  (ddev->fmem + DAQ1_CUR_BUF) = 0;
            * (uint32_t *) (ddev->fmem + AXI_CTRL_OUTD) = ctrl;
            mb();
            //Read ctrl once again to ensure that previous write was completed
            //by the PCIe layer.
            ctrl = * (uint32_t *) (ddev->fmem + AXI_CTRL_OUTD);
            ndelay(100);
            ctrl |= (1 << CTRL_OUTD_BIT_AP_nRST);
            * (uint32_t *) (ddev->fmem + AXI_CTRL_OUTD) = ctrl;
            spin_unlock_irqrestore(&ddev->gpio_lock,irqstate);
            return 0;
        }
        case DAQ1_CMD_START:
        {
            unsigned long irqstate;
            uint32_t ctrl;
            //We always start from the beging of the buffer and the first packet
            //Please rememember that DO_RESET should be done before!
            bitmap_zero(ddev->desc_to_confirm,DAQ1_NUM_EVT_DESCS);
            spin_lock_irqsave(&ddev->gpio_lock,irqstate);
            * (uint32_t *)  (ddev->fmem + DAQ1_CUR_PKT) = 0;
            * (uint32_t *)  (ddev->fmem + DAQ1_SRV_PKT) = 0;
            * (uint32_t *)  (ddev->fmem + DAQ1_CUR_BUF) = 0;
            ctrl = * (uint32_t *) (ddev->fmem + AXI_CTRL_OUTD);
            ctrl |= (1 << CTRL_OUTD_BIT_AP_START);
            ctrl |= (1 << CTRL_OUTD_BIT_SRC_START);
            * (uint32_t *) (ddev->fmem + AXI_CTRL_OUTD) = ctrl;
            spin_unlock_irqrestore(&ddev->gpio_lock,irqstate);
            return 0;
        }
        case DAQ1_CMD_DIS_IRQ:
            return wz_disable_irq(ddev);
        case DAQ1_CMD_STOP:
        {
            unsigned long irqstate;
            uint32_t ctrl;
            spin_lock_irqsave(&ddev->gpio_lock,irqstate);
            ctrl = * (uint32_t *) (ddev->fmem + AXI_CTRL_OUTD);
            ctrl &= ~(1 << CTRL_OUTD_BIT_AP_START);
            ctrl &= ~(1 << CTRL_OUTD_BIT_SRC_START);
            * (uint32_t *) (ddev->fmem + AXI_CTRL_OUTD) = ctrl;
            spin_unlock_irqrestore(&ddev->gpio_lock,irqstate);
            bitmap_zero(ddev->desc_to_confirm,DAQ1_NUM_EVT_DESCS);
            return 0;
        }
        }
        //regs->ctrl = arg;
        return 0;
    default:
        return -EINVAL;
    }
}


static int daq1_probe(struct pci_dev *pdev, const struct pci_device_id *ent)
{
    resource_size_t mmio_start, mmio_end, mmio_len;
    unsigned long mmio_flags;
    struct daq1_device * daq1_dev = NULL;
    int res = 0;
    // Make sure that the class is already registered
    if(! daq1_class_registered)
        return -EPROBE_DEFER;
    if(! ctrl1_class_registered)
        return -EPROBE_DEFER;
    daq1_dev = kzalloc(sizeof(*daq1_dev), GFP_KERNEL);
    if(! daq1_dev)
        return -ENOMEM;
    mutex_init(&daq1_dev->is_open);
    mutex_init(&daq1_dev->ctrl_is_open);
    mutex_init(&daq1_dev->confirm_lock);
    spin_lock_init(&daq1_dev->gpio_lock);
    res = daq1_get_minor(daq1_dev);
    if(res) {
        kfree(daq1_dev);
        return res;
    }
    daq1_dev->pdev = pdev;
    res =  pci_enable_device(pdev);
    if (res) {
        dev_err(&pdev->dev, "Can't enable PCI device, aborting\n");
        res = -ENODEV;
        goto err1;
    }
    mmio_start = pci_resource_start(pdev, 2);
    mmio_end = pci_resource_end(pdev, 2);
    mmio_flags = pci_resource_flags(pdev, 2);
    mmio_len = pci_resource_len(pdev, 2);
    daq1_dev->irq = pdev->irq;
    printk(KERN_INFO " mmio_start=%llx, mmio_end=%llx, mmio_len=%llx, irq=%d\n",mmio_start,mmio_end,mmio_len,daq1_dev->irq);
    /* make sure PCI base addr 1 is MMIO */
    if (!(mmio_flags & IORESOURCE_MEM)) {
        dev_err(&pdev->dev, "region #2 not an MMIO resource, aborting\n");
        res = -ENODEV;
        goto err2;
    }
    res = pci_request_regions(pdev, DEVICE_NAME);
    if (res) {
        dev_err(&pdev->dev, "Couldn't request PCI regions, aborting\n");
        goto err2;
    }
    pci_set_master(pdev);
    if (dma_set_mask_and_coherent(&pdev->dev, DMA_BIT_MASK(64))) {
        dev_err(&pdev->dev, "Couldn't set DMA mask, aborting\n");
        res = -ENODEV;
        goto err3;
    }
    res = dma_set_max_seg_size(&pdev->dev,HP_SIZE);
    if (res) {
        dev_err(&pdev->dev, "Couldn't set DMA mask, aborting\n");
        goto err3;
    }
    /* Create pointer needed to access registers */
    daq1_dev->phys_addr = mmio_start; //Save start address for further mapping
    daq1_dev->fmem = ioremap(mmio_start, mmio_len);
    if(! daq1_dev->fmem) {
        dev_err(&pdev->dev, "Couldn't remap registers for PCI device, aborting\n");
        res= -ENOMEM;
        goto err3;
    }
    /* Now we may check if this is a compatible device */
    {
        uint32_t id = * (uint32_t *) (daq1_dev->fmem + AXI_ID_IND);
        if (id != AXI_ID_VAL) {
            printk(KERN_ERR " expected AXI ID: %x, found: %x\n",(uint32_t) AXI_ID_VAL,id);
            goto err3;
        }
        mmio_start = pci_resource_start(pdev, 4);
        mmio_end = pci_resource_end(pdev, 4);
        mmio_flags = pci_resource_flags(pdev, 4);
        mmio_len = pci_resource_len(pdev, 4);
        printk(KERN_INFO " agwb_start=%llx, agwb_end=%llx, agwb_len=%llx\n",mmio_start,mmio_end,mmio_len);
        /* make sure PCI base addr 1 is MMIO */
        if (!(mmio_flags & IORESOURCE_MEM)) {
            dev_err(&pdev->dev, "region #4 not an MMIO resource, aborting\n");
            res = -ENODEV;
            goto err3;
        }
    }
    daq1_dev->agwb_addr = mmio_start;
    daq1_dev->agwb_size = mmio_len;
    /* Release reset from the HLS part, to enable safe access to registers */
    {
        uint32_t ctrl = * (uint32_t *) (daq1_dev->fmem + AXI_CTRL_OUTD);
        ctrl |= (1 << CTRL_OUTD_BIT_AP_nRST);
        * (uint32_t *) (daq1_dev->fmem + AXI_CTRL_OUTD) = ctrl;
    }
    init_waitqueue_head(&daq1_dev->wait);
    pci_set_drvdata(pdev, (void *) daq1_dev);
    device_create(&daq1_class,NULL,MKDEV(daq1_major, daq1_dev->minor),NULL,"my_daq%d",daq1_dev->minor);
    device_create(&ctrl1_class,NULL,MKDEV(ctrl1_major, daq1_dev->minor),NULL,"my_ctrl%d",daq1_dev->minor);
    return 0;
err3:
    pci_release_regions(pdev);
err2:
    pci_disable_device(pdev);
err1:
    if(daq1_dev)
        kfree(daq1_dev);
    return res;
}

static struct pci_driver daq1_pci_driver = {
    .name		= DEVICE_NAME,
    .id_table	= daq1_pci_tbl,
    .probe		= daq1_probe,
    .remove		= daq1_remove,
};

/* Copied and reworked from uio.c */

static int daq1_get_minor(struct daq1_device *idev)
{
    int retval;

    mutex_lock(&idr_lock);
    retval = idr_alloc(&daq1_idr, idev, 0, DAQ1_MAX_DEVICES, GFP_KERNEL);
    if (retval >= 0) {
        idev->minor = retval;
        retval = 0;
    } else if (retval == -ENOSPC) {
        dev_err(&idev->pdev->dev, "too many daq1 devices\n");
        retval = -EINVAL;
    }
    mutex_unlock(&idr_lock);
    return retval;
}

/*
static void daq1_free_minor(unsigned long minor)
{
  mutex_lock(&idr_lock);
  idr_remove(&daq1_idr, minor);
  mutex_unlock(&idr_lock);
}
*/

static int daq1_major_init(void)
{
    static const char name[] = "wz_daq1";
    struct cdev *cdev = NULL;
    dev_t daq1_dev = 0;
    int result;

    result = alloc_chrdev_region(&daq1_dev, 0, DAQ1_MAX_DEVICES, name);
    if (result)
        goto out;

    result = -ENOMEM;
    cdev = cdev_alloc();
    if (!cdev)
        goto out_unregister;

    cdev->owner = THIS_MODULE;
    cdev->ops = &daq1_fops;
    kobject_set_name(&cdev->kobj, "%s", name);

    result = cdev_add(cdev, daq1_dev, DAQ1_MAX_DEVICES);
    if (result)
        goto out_put;

    daq1_major = MAJOR(daq1_dev);
    daq1_cdev = cdev;
    return 0;
out_put:
    kobject_put(&cdev->kobj);
out_unregister:
    unregister_chrdev_region(daq1_dev, DAQ1_MAX_DEVICES);
out:
    return result;
}

static void daq1_major_cleanup(void)
{
    if(daq1_major) {
        unregister_chrdev_region(MKDEV(daq1_major, 0), DAQ1_MAX_DEVICES);
        cdev_del(daq1_cdev);
        daq1_major = 0;
    }
}

static void release_daq1_class(void)
{
    if(daq1_class_registered) {
        daq1_class_registered = false;
        class_unregister(&daq1_class);
    };
    daq1_major_cleanup();
}

static int ctrl1_major_init(void)
{
    static const char name[] = "wz_ctrl1";
    struct cdev *cdev = NULL;
    dev_t ctrl1_dev = 0;
    int result;

    result = alloc_chrdev_region(&ctrl1_dev, 0, DAQ1_MAX_DEVICES, name);
    if (result)
        goto out;

    result = -ENOMEM;
    cdev = cdev_alloc();
    if (!cdev)
        goto out_unregister;

    cdev->owner = THIS_MODULE;
    cdev->ops = &ctrl1_fops;
    kobject_set_name(&cdev->kobj, "%s", name);

    result = cdev_add(cdev, ctrl1_dev, DAQ1_MAX_DEVICES);
    if (result)
        goto out_put;

    ctrl1_major = MAJOR(ctrl1_dev);
    ctrl1_cdev = cdev;
    return 0;
out_put:
    kobject_put(&cdev->kobj);
out_unregister:
    unregister_chrdev_region(ctrl1_dev, DAQ1_MAX_DEVICES);
out:
    return result;
}

static void ctrl1_major_cleanup(void)
{
    if(ctrl1_major) {
        unregister_chrdev_region(MKDEV(ctrl1_major, 0), DAQ1_MAX_DEVICES);
        cdev_del(ctrl1_cdev);
        ctrl1_major = 0;
    }
}

static void release_ctrl1_class(void)
{
    if(ctrl1_class_registered) {
        ctrl1_class_registered = false;
        class_unregister(&ctrl1_class);
    };
    ctrl1_major_cleanup();
}

static int init_daq1_classes(void)
{
    int ret;
    /* This is the first time in here, set everything up properly */
    ret = daq1_major_init();
    if (ret)
        goto exit;
    ret = ctrl1_major_init();
    if (ret)
        goto exit;

    ret = class_register(&daq1_class);
    if (ret) {
        printk(KERN_ERR "class_register failed for DAQ\n");
        goto exit;
    }
    daq1_class_registered = true;
    ret = class_register(&ctrl1_class);
    if (ret) {
        printk(KERN_ERR "class_register failed for CTRL\n");
        goto exit;
    }
    ctrl1_class_registered = true;
    return 0;

exit:
    release_daq1_class();
    release_ctrl1_class();
    return ret;
}

static int __init daq1_init_module(void)
{
    int res;
    res = init_daq1_classes();
    if(res) return res;
    return pci_register_driver(&daq1_pci_driver);
}


static void __exit daq1_cleanup_module(void)
{
    pci_unregister_driver(&daq1_pci_driver);
    release_daq1_class();
    release_ctrl1_class();
    idr_destroy(&daq1_idr);
}


module_init(daq1_init_module);
module_exit(daq1_cleanup_module);

