import zmq
import os
import time
ctx=zmq.Context()
sdta=ctx.socket(zmq.PUSH)
sdta.bind("ipc:///tmp/feeds/dta")
sres=ctx.socket(zmq.PULL)
sres.bind("ipc:///tmp/feeds/res")
sctl=ctx.socket(zmq.PUB)
sctl.bind("ipc:///tmp/feeds/ctl")

# Start workers
for i in range(0,10):
    os.system("./zmq_worker &")
# Send them tasks and collect results
for i in range(0,20):
    sdta.send_string("Task:"+str(i)+"\0")
    print(sres.recv_string())
time.sleep(1)
sctl.send_string("END")

