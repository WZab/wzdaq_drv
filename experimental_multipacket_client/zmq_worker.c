#include <czmq.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(int argc, char * argv[])
{
    char msg[1000];
    char msg2[2000];
    pid_t pid;
    int nbytes;
    zmq_pollitem_t zmqs[2];
    pid = getpid();
    printf("Worker %d started\n",pid);
    void * ctx = zmq_ctx_new();
    void * pull = zmq_socket(ctx, ZMQ_PULL);
    int rc = zmq_connect(pull, "ipc:///tmp/feeds/dta");
    if(rc) {
        perror("I can't bind to the ZMQ PULL socket");
        return 1;
    }
    void * push = zmq_socket(ctx, ZMQ_PUSH);
    rc = zmq_connect(push, "ipc:///tmp/feeds/res");
    if(rc) {
        perror("I can't bind to the ZMQ PUSH socket");
        return 1;
    }
    void * ctl= zmq_socket(ctx, ZMQ_SUB);
    rc = zmq_connect(ctl, "ipc:///tmp/feeds/ctl");
    if(rc) {
        perror("I can't bind to the ZMQ SUB socket");
        return 1;
    }
    rc = zmq_setsockopt(ctl,ZMQ_SUBSCRIBE,NULL,0);
    if(rc) {
        perror("I can't subscribe the ZMQ SUB socket");
        return 1;
    }
    while(1) {
        memset(zmqs,0,sizeof(zmqs));
        zmqs[0].socket = pull;
        zmqs[0].events = ZMQ_POLLIN;
        zmqs[1].socket = ctl;
        zmqs[1].events = ZMQ_POLLIN;
        rc = zmq_poll(zmqs,2,-1);
        if(zmqs[0].revents & ZMQ_POLLIN) {
            nbytes = zmq_recv(pull,msg,1000,0);
            printf("Worker %d received %s\n",pid,msg);
            sprintf(msg2,"Worker %d processed %s",pid,msg);
            zmq_send(push,msg2,strlen(msg2)+1,0);
        }
        if (zmqs[1].revents & ZMQ_POLLIN) {
            printf("Worker %d finished\n",pid);
            zmq_close(pull);
            zmq_close(push);
            zmq_close(ctl);
            zmq_ctx_destroy(ctx);
            return 0;
        }
    }
}
