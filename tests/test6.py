#!/usr/bin/python3
import zmq
import ctypes
import struct
import random

def msg(nwords):
    res=b"WZDAQ1-D"  
    res+=struct.pack(">Q",nwords)
    #for i in range(0,nwords):
    #    res += struct.pack(">Q",random.randint(0,(1<<64)-1))
    res += struct.pack("<"+nwords*"Q",*[l for l in range(0,nwords)])
    return res
sks=[]
for i in range(0,8):
    ctx=zmq.Context()
    sk=ctx.socket(zmq.PAIR)
    sk.connect("tcp://localhost:300"+str(i))
    sks.append(sk)

i=0
while True:
    i=(i+1) % 8
    sk = sks[i]
    #b=[ctypes.c_uint64(i) for i in [3,57,5]]
    c=msg(500)
    c+=b"WZDAQ1-E"
    c+=msg(1022)
    sk.send(c)
    c=msg(3000000)
    sk.send(c)
    c=b"WZDAQ1-E"
    c+=msg(20000)
    c+=b"WZDAQ1-E"
    c+=msg(5133)
    c+=b"WZDAQ1-E"
    #c+=b"WZDAQ1-Q"
    sk.send(c)
    c=msg(7000)
    c+=b"WZDAQ1-E"
    sk.send(c)



