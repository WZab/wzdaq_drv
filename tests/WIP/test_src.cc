/*

  This is a high-speed test data source for the emulated DAQ.

  Copyright (C) 2021 by Wojciech M. Zabolotny  wzab<at>ise.pw.edu.pl
  Licensed under GPL v2

  Development of the driver and application is partially supported by the
  European Union’s Horizon 2020 research and innovation programme
  under grant agreement No 871072.

*/

#include <iostream>
#include <endian.h>
#include <unistd.h>
#include <sys/sysinfo.h>
#include <fstream>
#include <random>
#include <string>
#include <cstdint>
#include <functional>
#include <zmq.hpp>

std::default_random_engine rnge;
std::uniform_int_distribution<uint64_t> dist(0,0xFFFFffffFFFFffffll);
auto rndlw = std::bind ( dist, rnge);
// Create a msgpack message for DAQ
void msg_start(std::stringstream &buffer, uint64_t nwords)
{
 std::string header("WZDAQ1-D");
 uint64_t nw = htobe64(nwords);
 buffer.write(header.c_str(),header.length());
 buffer.write((const char *) &nw,sizeof(nw));  
 for (int i=0;i<nwords;i++) {
   uint64_t rn = rndlw();
   buffer.write((const char *)&rn,sizeof(rn));
   }   
}

void msg_end(std::stringstream &buffer)
{
 std::string trailer("WZDAQ1-E");
 buffer.write(trailer.c_str(),trailer.length());   
}

int main(int argc, char * argv[])
{
   std::ofstream outf;
   std::string pp("tcp://localhost:300");
   pp += argv[1];
   std::cout << pp << std::endl;
   zmq::context_t ctx;
   zmq::socket_t sock(ctx, zmq::socket_type::pair);
   sock.connect(pp);
   std::stringstream m1,m2,m3;
   msg_start(m1,1000);
   msg_end(m1);
   msg_start(m2,3000000);
   msg_end(m1);
   msg_start(m3,512000);
   msg_end(m1);
   //outf.open("of.bin");
   //outf << m1.rdbuf();
   //outf.close();
   while(1) {
      if(!sock.send(zmq::buffer(m1.str()), zmq::send_flags::none))
         usleep(1000);
      if(!sock.send(zmq::buffer(m2.str()), zmq::send_flags::none))
         usleep(1000);
      if(!sock.send(zmq::buffer(m3.str()), zmq::send_flags::none))
         usleep(1000);
      while(get_avphys_pages() < 1000000) {
        // Wait until there is enough free memory...
        usleep(1000);
        };
      }
}

