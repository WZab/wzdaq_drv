#!/usr/bin/python3
import zmq
import ctypes
import struct
import random
import sys
import time

# Generate messages with verification
def msg(last=False):
    if last:
        res=b"WZDAQ1-T"  
    else:
        res=b"WZDAQ1-D"  
    res += struct.pack("<QQQ",0,0,0)
    #for i in range(0,nwords):    
    #    res += struct.pack(">Q",random.randint(0,(1<<64)-1))
    length = random.randint(400,30000)
    init = random.randint(0,100)
    step = random.randint(0,50);
    res += struct.pack("<QQQQ",0x7e57da7a32abb3a9,length,init,step)
    res += struct.pack("<"+4*length*"Q",*[init + step * l for l in range(0,4*length)])
    return res

ctx=zmq.Context()
sk=ctx.socket(zmq.PAIR)
sk.connect("tcp://localhost:300"+sys.argv[1])
#b=[ctypes.c_uint64(i) for i in [3,57,5]]
for i in range(0,67000):
  c=msg(True)
  sk.send(c)
#  time.sleep(3)



