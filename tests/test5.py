#!/usr/bin/python3
import zmq
import ctypes
import struct
import random
import sys

def msg(nwords,last=False):
    if last:
        res=b"WZDAQ1-T"  
    else:
        res=b"WZDAQ1-D"  
    res += struct.pack("<QQQ",0,0,0)
    #for i in range(0,nwords):    
    #    res += struct.pack(">Q",random.randint(0,(1<<64)-1))
    res += struct.pack("<"+4*nwords*"Q",*[l for l in range(0,4*nwords)])
    return res

ctx=zmq.Context()
sk=ctx.socket(zmq.PAIR)
sk.connect("tcp://localhost:300"+sys.argv[1])
#b=[ctypes.c_uint64(i) for i in [3,57,5]]
for i in range(0,22000):
  c=msg(10000)
  sk.send(c)
  c=msg(5000,True)
  sk.send(c)
  c=msg(10011,True)
  sk.send(c)
  c=msg(10011)
  sk.send(c)
  c=msg(10011)
  sk.send(c)
  c=msg(5000,True)
  sk.send(c)
  c=msg(400)
  sk.send(c)
  c=msg(1022,True)
  sk.send(c)
  #c=msg(3000000)
  #sk.send(c)
  c=msg(22000)
  sk.send(c)
  c=msg(5133,True)
  sk.send(c)
  c=msg(633,True)
  sk.send(c)
  c=msg(7000,True)
  sk.send(c)



